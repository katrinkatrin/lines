﻿package  {
	
	import flash.display.MovieClip;
	import fl.controls.Button;
	import flash.events.MouseEvent;
	
	public class MainMenu extends MovieClip {
		
		/*private var newGameBtn:newgame_btn;
		private var loadGameBtn:loadgame_btn;
		private var settingsBtn:settings_btn;
		private var exitBtn:exit_btn;*/
		
		private var newGameBtn:Button;
		private var loadGameBtn:Button;
		private var settingsBtn:Button;
		private var exitBtn:Button;
		
		private var gameScene:GameScene;
		private var gameOver:GameOverWnd;
		
		public function MainMenu() {
			gameScene = new GameScene(setGameOver);
			gameOver = new GameOverWnd();
			
			newGameBtn = new Button();
			newGameBtn.label = "New Game";
			newGameBtn.width = 200;
			newGameBtn.height = 30;
			newGameBtn.move(400, 200);
			
			loadGameBtn = new Button();
			loadGameBtn.label = "Load Game";
			loadGameBtn.width = 200;
			loadGameBtn.height = 30;
			loadGameBtn.move(400, 270);
			
			settingsBtn = new Button();
			settingsBtn.label = "Settings";
			settingsBtn.width = 200;
			settingsBtn.height = 30;
			settingsBtn.move(400, 340);
			
			exitBtn = new Button();
			exitBtn.label = "Exit";
			exitBtn.width = 200;
			exitBtn.height = 30;
			exitBtn.move(400, 410);
			
			newGameBtn.addEventListener(MouseEvent.CLICK,newGame);
			loadGameBtn.addEventListener(MouseEvent.CLICK,loadGame);
			settingsBtn.addEventListener(MouseEvent.CLICK,loadSettings);
			exitBtn.addEventListener(MouseEvent.CLICK,exitGame);
			
			addChild(newGameBtn);
			addChild(loadGameBtn);
			addChild(settingsBtn);
			addChild(exitBtn);
		}
		
		public function newGame(e:MouseEvent):void
		{
			addChild(gameScene);
			gameScene.newGame();
		}

		public function loadGame(e:MouseEvent):void
		{

		}
		
		public function loadSettings(e:MouseEvent):void
		{

		}
		
		public function exitGame(e:MouseEvent):void
		{

		}
		
		public function setGameOver():void
		{
			removeChild(gameScene);
			addChild(gameOver);
		}		
		
	}
	
}
