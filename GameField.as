﻿package  {
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	
	public class GameField extends Sprite{
		private var FIELD_SIZE:uint = 9;
		private var _logic:Logic = new Logic();
		
		private var _cells:Array = new Array();
		private var _ballItems:Array = new Array();
		
		private var _selectedBall:Boolean = false;
		private var _selectedCell:Boolean = false;
		private var _moving:Boolean = false;
		private var _gameOver:Boolean = false;
		
		private var _ballItemSelected:BallItem;
		private var _cellSelected:Cell;
		
		private var _scores:int = 0;
		
		private var _changeScores:Function;
		private var _setGameOver:Function;
		
		public function GameField(changeScores:Function, setGameOver:Function) {
			_changeScores = changeScores;
			_setGameOver = setGameOver;
			fillCells();
			addBalls();
			
			addEventListener(MouseEvent.CLICK, testClick);
		}

		public function testClick(e:MouseEvent)
		{
			trace(e.stageX + "		" + e.stageY);
			stageClick(e.stageX, e.stageY);
		}
		
		private function fillCells():void{
			var j:uint;
			var i:uint;
			
			for (i = 0; i < FIELD_SIZE; i++) 
			{ 
				_cells[i] = ["0","0","0","0","0","0","0","0","0"];
			}
			
		}
		
		private function addBall(x:uint, y:uint, type:int):void{
			var bi:BallItem = new BallItem(_logic.convertCellToScreenX(x), 
											_logic.convertCellToScreenY(y), type);
			trace("addBall: " + x + "	" + y);
			_ballItems[y*FIELD_SIZE+x] = bi;
			_ballItems[y*FIELD_SIZE+x].ball().addEventListener(MouseEvent.CLICK, selectBall);
			addChild(_ballItems[y*FIELD_SIZE+x].ball());
			_cells[x][y] = _ballItems[y*FIELD_SIZE+x].getType();
			//printCell();
		}
		
		private function getIndex():uint{
			return Math.floor(Math.random()*8);
		}
		
		
		public function addBalls():void{
			var i:uint;
			for(i = 0; i < 3; i++)
			{
				var x:uint = 0;
				var y:uint = 0;
				var type:int = 0;
				do
				{
					x = Math.floor(Math.random()*8);
					y = Math.floor(Math.random()*8);
					type = Math.floor(Math.random()*5) + 1;
				} while(_cells[x][y] != 0)
				
				addBall(x,y, type);
			}
		}
		
		// true - exist 3 Cells
		private function existEmptyCells():Boolean
		{
			
			var emptyCount:uint = 0;
			for (var j:uint = 0; j < FIELD_SIZE; j++) {
				for (var i:uint = 0; i < FIELD_SIZE; i++) {
					if(_cells[i][j] == 0)
					{
						emptyCount = emptyCount + 1;
						if(emptyCount == 3)
							return true;
					}
				}
			}
			return false;
		}
		
		public function ballItems():Array{
			return _ballItems;
		}
		
		// x, y экранные координаты
		public function selectBall(e:MouseEvent):void{
			var x:uint = e.stageX;
			var y:uint = e.stageY;
			
			if(_selectedCell)
				return;
			
			_selectedBall = true;
			
			var xCell:int = _logic.convertScreenToCellX(x);
			var yCell:int = _logic.convertScreenToCellY(y);
			_ballItemSelected = _ballItems[yCell*FIELD_SIZE+xCell];
		}
		
		public function stageClick(x:uint, y:uint):void{
			if((_logic.convertScreenToCellY(y) != -1)
				&& (_logic.convertScreenToCellX(x) != -1))
			{
				if(!_selectedBall)
					return;
				
				if(_cells[_logic.convertScreenToCellX(x)][_logic.convertScreenToCellY(y)] != 0)
					return;
				
				if(_moving)
					return;
				
				_selectedCell = true;
				
				_cellSelected = new Cell(_logic.convertScreenToCellX(x), _logic.convertScreenToCellY(y));
				
				if(tryMove())
				{
					if(!tryDeleteBalls())
					{
						if(existEmptyCells())
						{
							addBalls();
							tryDeleteBalls();
						}
						else
						{
							_gameOver = true;
							_setGameOver();
						}
					}
				}
			}
		}
		
		public function tryDeleteBalls():Boolean
		{
			var deletedItems:int = _logic.deleteBalls(_cells);
			if(deletedItems > 0)
			{
				var items:Vector.<Cell> = _logic.deletedItems;
				for(var i:int = 0; i<items.length; i++)
				{
					var cell:Cell = items[i];
					_cells[cell.i][cell.j] = 0;	
					_ballItems[cell.j*FIELD_SIZE+cell.i].ball().removeEventListener(MouseEvent.CLICK, selectBall);
					removeChild(_ballItems[cell.j*FIELD_SIZE+cell.i].ball());
				}	
				_scores += deletedItems;
				_changeScores();
				_logic.cleanDeletedItems();
				return true;
			}
			
			return false;
		}
		
		public function tryMove():Boolean
		{
			if(_selectedCell && _selectedBall)
			{
				if(findRoute())
				{
					_moving = true;
					
					var prevBallPosXSc:uint = _ballItemSelected.xPosition();
					var prevBallPosYSc:uint = _ballItemSelected.yPosition();
					var prevBallPosX:uint = _logic.convertScreenToCellX(_ballItemSelected.xPosition());
					var prevBallPosY:uint = _logic.convertScreenToCellY(_ballItemSelected.yPosition());
					
					var type:int = _ballItems[prevBallPosY*FIELD_SIZE+prevBallPosX].getType();
					
					_cells[_cellSelected.i][_cellSelected.j] = type;					
					_cells[prevBallPosX][prevBallPosY] = 0;
					
					moveBall();
					
					addBall(_cellSelected.i, _cellSelected.j, type);
					_ballItems[prevBallPosY*FIELD_SIZE+prevBallPosX].ball().removeEventListener(MouseEvent.CLICK, selectBall);
					removeChild(_ballItems[prevBallPosY*FIELD_SIZE+prevBallPosX].ball());
					
					_ballItemSelected = null;					
					_selectedCell = false;
					_selectedBall = false;
					_moving = false;
					//printCell();
					return true;
				}
				else
				{
					_selectedCell = false;
					return false;
				}
				
			}
			return false;
		}
		
		public function printCell():void
		{
			trace("-----printCell------");
			for (var i:int = 0; i < FIELD_SIZE; i++) 
			{ 
				trace(_cells[i]);
			}
		}
		
		public function findRoute():Boolean
		{
			return _logic.findRoute(_cells, _ballItemSelected, _cellSelected);
		}
		
		public function moveBall():void
		{
			var path:Vector.<Cell> = _logic.route;
			//addEventListener(Event.ENTER_FRAME, animateMoving);
			for(var i:int = 0; i<path.length; i++)
			{
				_ballItemSelected.move(_logic.convertCellToScreenX(path[i].i), _logic.convertCellToScreenY(path[i].j));
			}
			_logic.cleanRoute();
		}
		/*
		private function animateMoving(event:Event):void 
        {
			trace("animateMoving");
            if (_logic.route.length > 0)
            {
				var cell:Cell = _logic.route[_logic.route.length - 1];
				trace("Cell i = "+cell.i+"	j = " + cell.j);
				ballItemSelected.move(convertCellToScreenX(cell.i), convertCellToScreenY(cell.j));
				_logic.route.pop();
            }
            else
            {
				//_logic.cleanRoute();
                removeEventListener(Event.ENTER_FRAME, animateMoving);
            }
        }*/
		
		public function getScore():int{
			return _scores;
		}
		
	}
	
}
