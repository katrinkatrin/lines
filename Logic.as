﻿package  {
	
	public class Logic {
		private var FIELD_SIZE:uint = 9;
		private var _tempField:Array;
		private var _route:Vector.<Cell> = new Vector.<Cell>();
		private var _routeDetected:Boolean = false;
		private var _needToDelete:Vector.<Cell> = new Vector.<Cell>();
		
		private var _startCell:Cell;
		private var _endCell:Cell;
		
		public function Logic() {
		}
		
		public function findRoute(_cells:Array, ballItemSelected:BallItem, cellSelected:Cell):Boolean
		{
			trace("Logic: findRoute");			
			trace("cells :" + _cells);
			
			_routeDetected = false;

			_tempField = new Array();
			for (var l:uint = 0; l < FIELD_SIZE; l++) 
			{ 
				_tempField[l] = ["0","0","0","0","0","0","0","0","0"];
			}
			
			for (var m:uint = 0; m < FIELD_SIZE; m++) 
			{
				for (var n:uint = 0; n < FIELD_SIZE; n++) 
				{
					if(_cells[n][m] == -1)
						_tempField[n][m] = -1;
				}
			}
			
			_tempField[convertScreenToCellX(ballItemSelected.xPosition())][convertScreenToCellY(ballItemSelected.yPosition())] = 1;
			_startCell = new Cell(convertScreenToCellX(ballItemSelected.xPosition()), 
									convertScreenToCellY(ballItemSelected.yPosition()));
			_endCell = new Cell(cellSelected.i, cellSelected.j);
			
			var d:int = 1;
			var end:Boolean = false;
			
			var detected:Boolean = false;
			var i:int = 0;
			var j:int = 0;
			do
			{
			detected = false;
			for (j = 0; j < FIELD_SIZE; j++) {
				for (i = 0; i < FIELD_SIZE; i++) {
					if(_tempField[i][j] == d)
					{
						//detected
						if((i == cellSelected.i) && (j == cellSelected.j))
						{
							trace("Find cell: i = " + i + "		" + j);
							end = true;
							_routeDetected = true;
						}
						
						if((i-1)>=0 && (i-1)<=8 && j>=0 && j<=8)
						{
							if(_tempField[i-1][j] == 0)
							{
								_tempField[i-1][j] = d + 1;	
								detected = true;
							}
						}
						
						if((i+1)>=0 && (i+1)<=8 && j>=0 && j<=8)
						{
							if(_tempField[i+1][j] == 0)
							{
								_tempField[i+1][j] = d + 1;
								detected = true;
							}
						}
						
						if(i>=0 && i<=8 && (j-1)>=0 && (j-1)<=8)
						{
							if(_tempField[i][j-1] == 0)
							{
								_tempField[i][j-1] = d + 1;
								detected = true;
							}
						}
						
						if(i>=0 && i<=8 && (j+1)>=0 && (j+1)<=8)
						{
							if(_tempField[i][j+1] == 0)
							{
								_tempField[i][j+1] = d + 1;
								detected = true;
							}
						}
					}
				}
			}
			d = d + 1;
		} while(!end && detected);
			d = d - 1;
			//trace("d = " + (d));
			//trace("_routeDetected = " + _routeDetected);
			//trace("temp  :" + _tempField);
			//trace("----------------");
			createRoute();
			return _routeDetected;
		}
		
		public function createRoute():void
		{
			if(!_routeDetected)
				return;
			
			var d:int = _tempField[_endCell.i][_endCell.j];
			var startReceived:Boolean = false;
			
			var i:int = _endCell.i;
			var j:int = _endCell.j;
			
			var cell:Cell = new Cell(i,j);
			_route.push(cell);
			
			trace("----------");

			do{					
					if((i-1)>=0 && (i-1)<=8 && j>=0 && j<=8)
					{
						if(_tempField[i-1][j] == (d-1))
						{
							cell = new Cell(i-1,j);
							_route.push(cell);
							d = d-1;
							i = i-1;
							continue;
						}
					}
					if((i+1)>=0 && (i+1)<=8 && j>=0 && j<=8)
					{
						if(_tempField[i+1][j] == (d-1))
						{
							cell = new Cell(i+1,j);
							_route.push(cell);
							d = d-1;
							i = i+1;
							continue;
						}
					}
					if(i>=0 && i<=8 && (j-1)>=0 && (j-1)<=8)
					{
						if(_tempField[i][j-1] == (d-1))
						{
							cell = new Cell(i,j-1);
							_route.push(cell);
							d = d-1;
							j = j-1;
							continue;
						}
					}
					if(i>=0 && i<=8 && (j+1)>=0 && (j+1)<=8)
					{
						if(_tempField[i][j+1] == (d-1))
						{
							cell = new Cell(i,j+1);
							_route.push(cell);
							d = d-1;
							j = j+1;
							continue;
						}
					}
					
					if((cell.i == _startCell.i) && (cell.j == _startCell.j))
						startReceived = true;
					
			} while(!startReceived)
			
			_route.reverse();
			//for(var r:int; r<_route.length; r++)
			//{
			//	trace("Route = " + _route[r].i + "	"+ _route[r].j);
			//}
			//trace("-------------");
		}
		
		public function deleteBalls(cells:Array):uint
		{
			var j,i,k, m:int;
			for (j = 0; j < FIELD_SIZE; j++) {
				
				var count:uint = 1;
				var value:int = 0;
				
				for (i = 0; i < FIELD_SIZE; i++) {
					if((cells[i][j] != 0) && 
						(cells[i][j] == value))
					{
						count = count + 1;
					}
					else if((cells[i][j] != value) &&
						(count>=5))
					{
						for(k = i-1; k > i - count - 1; k--)
						{
							var isExist = false;
							for(m = 0; m < _needToDelete.length; m++)
							{
								if((_needToDelete[m].i == k) &&
								(_needToDelete[m].j == j))
									isExist = true;
							}
							
							if(!isExist)
							{
								_needToDelete.push(new Cell(k,j));
							}
						}
						
						count = 1;
					}
					else if((cells[i][j] != 0) && 
						(cells[i][j] != value) &&
						(count<5))
					{
						count = 1;
					}
					value = cells[i][j];
				}
			}
			
			for (j = 0; j < FIELD_SIZE; j++) {
				
				var count:uint = 1;
				var value:int = 0;
				
				for (i = 0; i < FIELD_SIZE; i++) {
					if((cells[j][i] != 0) && 
						(cells[j][i] == value))
					{
						count = count + 1;
					}
					else if((cells[j][i] != value) &&
						(count>=5))
					{
						for(k = i-1; k > i - count - 1; k--)
						{
							var isExist = false;
							for(m = 0; m < _needToDelete.length; m++)
							{
								if((_needToDelete[m].i == j) &&
								(_needToDelete[m].j == k))
									isExist = true;
							}
							
							if(!isExist)
								_needToDelete.push(new Cell(j,k));
						}
						count = 1;
					}
					else if((cells[j][i] != 0) && 
						(cells[j][i] != value) &&
						(count<5))
					{
						count = 1;
					}
					value = cells[j][i];
				}
			}
			
			return _needToDelete.length;
		}
		
		public function get deletedItems():Vector.<Cell>
		{
			return _needToDelete;
		}

		public function get route():Vector.<Cell>
		{
			return _route;
		}

		public function cleanRoute():void
		{
			_route.length = 0;
		}
		
		public function cleanDeletedItems():void
		{
			_needToDelete.length = 0;
		}
		
		public function convertScreenToCellX(x:uint):int{
			if(x < 230 || x > 775)
				return -1;
			
			return (x - 230) / 60;
		}

		public function convertScreenToCellY(y:uint):int{
			if(y < 200 || y > 745)
				return -1;
			
			return (y - 200) / 60;
		}
		
		public function convertCellToScreenX(x:uint):int{
			if(x > FIELD_SIZE)
				return -1;
			
			return 230 + x * 60 + 30;
		}
		
		public function convertCellToScreenY(y:uint):int{
			if(y > FIELD_SIZE)
				return -1;
			
			return 200 + y * 60 + 30;
		}


	}
	
}
