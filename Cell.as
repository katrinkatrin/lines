﻿package  {
	
	public class Cell {

		private var _i:uint;
		private var _j:uint;
		
		public function Cell(i:uint, j:uint) {
			_i = i;
			_j = j;
		}
		
		public function get i():uint
		{
			return _i;
		}
		
		public function get j():uint
		{
			return _j;
		}		
	}
	
}
