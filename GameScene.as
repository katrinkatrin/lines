﻿package  {
	import flash.display.Sprite;
	import flash.text.TextField;
    import flash.text.TextFieldType;
	import flash.text.TextFieldAutoSize;
    import flash.text.AntiAliasType;
    import flash.text.GridFitType;
    import flash.text.TextFormat;
	import flash.text.StyleSheet;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class GameScene extends Sprite{

		private var _newGameField:GameField;
		private var _score:int;
		private var _time:int;
		
		private var _scoreField:TextField;
		private var _timerField:TextField;
		
		private var _setGameOver:Function;
		
		private var _secTimer:Timer = new Timer(1000, 1);
		
		public function GameScene(setGameOver:Function) {
			_setGameOver = setGameOver;
			_newGameField = new GameField(setScores, _setGameOver);
			_scoreField = createCustomTextField(750, 40, 700, 30);
			_timerField = createCustomTextField(30, 40, 700, 30);
			_secTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
		}

		public function newGame():void
		{
			addChild(_newGameField);
			addChild(_scoreField);
			addChild(_timerField);
			setScores();
			_secTimer.start();
		}
		
		public function onTimerComplete(event:TimerEvent):void 
        { 
			_time = _time + 1;
			var date:Date = new Date(null, null, null, 0, 0, _time);

			var str:String = "Time: ";
			
			var hours = date.getHours();
			var min = date.getMinutes();
			var sec = date.getSeconds();
			
			if(hours == 0)
				str = str + "00:";
			else
				str = str + "0" + hours + ":";
			
			if(min == 0)
				str = str + "00:";
			else if(min < 10)
				str = str + "0" + min + ":";
			else
				str = str + String(min) + ":";
			
			if(sec == 0)
				str = str + "00";
			else if(sec < 10)
				str = str + "0" + sec;
			else
				str = str + String(sec);
			
			_timerField.text =  str;
			_secTimer.start();
        }
		
		public function setScores():void
		{
			_scoreField.text = String("Scores: " + _newGameField.getScore());
		}
		/*
		public function setGameOver():void
		{
			
		}*/
		
		private function createCustomTextField(x:Number, y:Number, width:Number, height:Number):TextField {
            var result:TextField = new TextField();
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = 25;
            result.x = x; 
			result.y = y;
            result.width = width; 
			result.height = height;
			result.textColor = 0xffa64d;
			result.defaultTextFormat = textFormat;
            return result;
        }
	}
	
}
