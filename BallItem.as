﻿package  {
	
	import flash.utils.getDefinitionByName;
	import flash.display.MovieClip;
	
	public class BallItem {
		private var _width:uint = 55;
		private var _height:uint = 55;
		
		private var _xPosition:uint;
		private var _yPosition:uint;
		
		private var _color:uint;
		private var _type:int;
		
		private var _obj:MovieClip;
		
		public function BallItem(xPosition:uint, yPosition:uint, type:int) {
			_xPosition = xPosition;
			_yPosition = yPosition;			
			_type = type;
			
			switch(_type)
			{
				case 1:
					_obj = new Ball_blue;
					break;
				case 2:
					_obj = new Ball_cyan;
					break;
				case 3:
					_obj = new Ball_green;
					break;
				case 4:
					_obj = new Ball_magenta;
					break;
				case 5:
					_obj = new Ball_red;
					break;
				case 6:
					_obj = new Ball_yellow;
					break;
			}
			
			_obj.x = _xPosition;
			_obj.y = _yPosition;
		}
		

		public function createInstance(className:String):Object
		{
			var myClass:Class = getDefinitionByName(className) as Class;
			var instance:Object = new myClass();
			return instance;
		}

		public function move(xPosition:uint, yPosition:uint):void
		{
			_xPosition = xPosition;
			_yPosition = yPosition;
			_obj.x = _xPosition;
			_obj.y = _yPosition;
		}
		
		public function xPosition():uint
		{
			return _xPosition;
		}
		
		public function yPosition():uint
		{
			return _yPosition;
		}
		
		public function ball():Object
		{
			return _obj;
		}
		
		public function getType():int
		{
			return _type;
		}
		
	}
	
}
