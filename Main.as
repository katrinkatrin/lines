﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.TimerEvent; 
	import flash.events.MouseEvent;
    import flash.utils.Timer; 
	
	public class Main extends MovieClip {
		
		private var _intro:Intro;
		private var _mainMenu:MainMenu;
		
		public function Main() {
			var minuteTimer:Timer = new Timer(1000, 3);
			minuteTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete); 
			minuteTimer.start();
			
			_intro = new Intro();
			_mainMenu = new MainMenu();
			
			showIntro();
		}
		
		private function showIntro()
		{
			addChild(_intro);
			_intro.x = stage.stageWidth/2;
			_intro.y = stage.stageHeight/2;
		}
		
		public function onTimerComplete(event:TimerEvent):void 
        { 
			removeChild(_intro);
			addChild(_mainMenu);
        } 
		
		/*public function newGame():void
		{
			trace("newGame!");
			addChild(newGameField);
			//newGameField.x = stage.stageWidth/2;
			//newGameField.y = stage.stageHeight/2;
		}*/
	}
	
}
